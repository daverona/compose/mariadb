# daverona/docker-compose/mariadb

## Quick Start

```bash
cp .env.example .env  
# edit .env
```

If you want to initialize your instance with certain files (with extension `.sh`, `.sql`, and `.sql.gz`) 
when a container runs for the *first time*, copy files to `storage/mariadb/initdb.d` directory.

Run a container and secure your instance:

```bash
docker-compose up --detach
docker-compose exec mariadb mysql_secure_installation
# answer the questions based on your needs
```

Log in to mariadb:

```bash
docker-compose exec mariadb mysql --user=root --password=secret
```

## References

* MariaDB Server Documentation: [https://mariadb.com/kb/en/documentation/](https://mariadb.com/kb/en/documentation/)
* Binary Logging Options and Variables: [https://dev.mysql.com/doc/refman/8.0/en/replication-options-binary-log.html](https://dev.mysql.com/doc/refman/8.0/en/replication-options-binary-log.html)
* Server System Variables: [https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html](https://dev.mysql.com/doc/refman/8.0/en/server-system-variables.html)
* MariaDB repository: [https://github.com/MariaDB/server](https://github.com/MariaDB/server)
* MariaDB registry: [https://hub.docker.com/\_/mariadb](https://hub.docker.com/_/mariadb)
